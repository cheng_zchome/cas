# Requirement
The function of a **DVB** CAS system is to broadcast several types of binary messages inside a DVB stream. 
Each type of message is characterized by a desired cycling frequency and a reserved bandwidth. 
The sum of all messages bandwidth must stay under a predefined limit bandwidth whatever the number of messages in the system (1 Mb/s). 
Message sizes are between 100 and 256 bytes.
